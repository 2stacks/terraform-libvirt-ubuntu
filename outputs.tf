output "instance_id" {
  description = "Instance ID"
  value       = libvirt_domain.ubuntu.id
}

output "instance_name" {
  description = "Instance Name"
  value       = libvirt_domain.ubuntu.name
}

output "instance_ips" {
  description = "Instance IP Addresses"
  value       = libvirt_domain.ubuntu.network_interface[*].addresses
}

output "ipv4_address" {
  description = "IPv4 Address"
  value       = libvirt_domain.ubuntu.network_interface[0].addresses[0]
}

output "ipv6_address" {
  description = "IPv6 Address"
  value       = libvirt_domain.ubuntu.network_interface[0].addresses[1]
}

output "volume_id" {
  description = "Instance Volume ID"
  value       = libvirt_domain.ubuntu.disk[*].volume_id
}
