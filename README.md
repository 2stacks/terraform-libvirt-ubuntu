# terraform-libvirt-ubuntu

<!-- Description of module -->
This module creates Ubuntu Cloud Image KVM Virtual machines using [terraform-provider-libvirt](https://registry.terraform.io/providers/dmacvicar/libvirt/latest)

## References
<!-- Include links to external references -->
[terraform-provider-libvirt](https://github.com/dmacvicar/terraform-provider-libvirt)

## Table of Contents
- [Usage](#usage)
- [Requirements](#requirements)
- [Providers](#providers)
- [Modules](#modules)
- [Resources](#resources)
- [Inputs](#inputs)
- [Outputs](#outputs)
- [Contributing](#contributing)

## Usage
<!-- Describe manual and/or automated usage steps -->
See the [examples](/examples) folder for examples of how to utilize this module with Terraform.

### Pre-requisites
<!-- Describe external dependencies or pre-requisites -->
- [kvm](https://www.linux-kvm.org/page/Main_Page)
- [libvirt](https://libvirt.org/)
- [xsltproc](http://xmlsoft.org/xslt/xsltproc2.html)

#### NOTE
The host responsible for running this module must have `xsltproc` installed.  The following error indicates this package has not been installed.  Consult your OSs package manager or the link above for instructions on installing this dependency.
```bash
│ Error: error applying XSLT stylesheet: exec: "xsltproc": executable file not found in $PATH
│
│   with libvirt_domain.ubuntu[0],
│   on main.tf line 77, in resource "libvirt_domain" "ubuntu":
│   77: resource "libvirt_domain" "ubuntu" {
```

### Quick Start
```bash
terraform init
terraform plan
terraform apply
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.4.0 |
| <a name="requirement_libvirt"></a> [libvirt](#requirement\_libvirt) | ~> 0.7.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_libvirt"></a> [libvirt](#provider\_libvirt) | 0.7.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [libvirt_cloudinit_disk.commoninit](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/cloudinit_disk) | resource |
| [libvirt_domain.ubuntu](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/domain) | resource |
| [libvirt_volume.ubuntu_qcow2_resized](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/volume) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_hostname"></a> [hostname](#input\_hostname) | Guest Hostname | `string` | `"ubuntu"` | no |
| <a name="input_libvirt_volume_id"></a> [libvirt\_volume\_id](#input\_libvirt\_volume\_id) | The backing volume (CoW) to use for this volume | `string` | n/a | yes |
| <a name="input_libvirt_volume_pool"></a> [libvirt\_volume\_pool](#input\_libvirt\_volume\_pool) | Volume Storage Pool | `string` | `"default"` | no |
| <a name="input_libvirt_volume_size"></a> [libvirt\_volume\_size](#input\_libvirt\_volume\_size) | Volume Size in Bytes | `number` | `10737418240` | no |
| <a name="input_memory"></a> [memory](#input\_memory) | Memory in Megabytes | `string` | `"2048"` | no |
| <a name="input_meta_data"></a> [meta\_data](#input\_meta\_data) | cloud-init meta data | `string` | `""` | no |
| <a name="input_network"></a> [network](#input\_network) | Name of Libvirt Network | `string` | `"default"` | no |
| <a name="input_network_config"></a> [network\_config](#input\_network\_config) | cloud-init network-config data | `string` | `""` | no |
| <a name="input_port_group"></a> [port\_group](#input\_port\_group) | Namve of OVS Port Group | `string` | `"default"` | no |
| <a name="input_user_data"></a> [user\_data](#input\_user\_data) | cloud-init user data | `string` | `""` | no |
| <a name="input_vcpu"></a> [vcpu](#input\_vcpu) | Number of vCPUs | `string` | `"2"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_instance_id"></a> [instance\_id](#output\_instance\_id) | Instance ID |
| <a name="output_instance_ips"></a> [instance\_ips](#output\_instance\_ips) | Instance IP Addresses |
| <a name="output_instance_name"></a> [instance\_name](#output\_instance\_name) | Instance Name |
| <a name="output_ipv4_address"></a> [ipv4\_address](#output\_ipv4\_address) | IPv4 Address |
| <a name="output_ipv6_address"></a> [ipv6\_address](#output\_ipv6\_address) | IPv6 Address |
| <a name="output_volume_id"></a> [volume\_id](#output\_volume\_id) | Instance Volume ID |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Contributing
Code quality and security will be validated before merge requests are accepted.

### Tools
These tools are used to ensure validation and standardization of Terraform deployments

#### Must be installed
- [pre-commit](https://github.com/gruntwork-io/pre-commit/releases)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)

#### Provided by Terraform
- [terraform fmt](https://www.terraform.io/docs/commands/fmt.html)
- [terraform validate](https://www.terraform.io/docs/commands/validate.html)

For more information see - [pre-commit-hooks-for-terraform](https://medium.com/slalom-build/pre-commit-hooks-for-terraform-9356ee6db882)

### To submit a merge request
```bash
git checkout -b <branch name>
pre-commit autoupdate
pre-commit run -a
git commit -a -m 'Add new feature'
git push origin <branch name>
```
Optionally run the following to automate the execution of pre-commit on every git commit.
```bash
pre-commit install
```

# License
Copyright (c) 2022 [2stacks.net](www.2stacks.net)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
