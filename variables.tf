# Libvirt Instance Settings
variable "libvirt_volume_pool" {
  description = "Volume Storage Pool"
  type        = string
  default     = "default"
}

variable "libvirt_volume_id" {
  description = "The backing volume (CoW) to use for this volume"
  type        = string
}

variable "libvirt_volume_size" {
  description = "Volume Size in Bytes"
  type        = number
  default     = 10737418240
}

variable "hostname" {
  description = "Guest Hostname"
  type        = string
  default     = "ubuntu"
}

variable "memory" {
  description = "Memory in Megabytes"
  type        = string
  default     = "2048"
}

variable "vcpu" {
  description = "Number of vCPUs"
  type        = string
  default     = "2"
}

variable "network" {
  description = "Name of Libvirt Network"
  type        = string
  default     = "default"
}

variable "port_group" {
  description = "Namve of OVS Port Group"
  type        = string
  default     = "default"
}

# CloudInit Settings
variable "user_data" {
  description = "cloud-init user data"
  type        = string
  default     = ""
}

variable "meta_data" {
  description = "cloud-init meta data"
  type        = string
  default     = ""
}

variable "network_config" {
  description = "cloud-init network-config data"
  type        = string
  default     = ""
}
