terraform {
  required_version = "~> 1.4.0"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "~> 0.7.0"
    }
  }
}

# instance the provider
provider "libvirt" {
  uri = var.libvirt_uri
}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "ubuntu_qcow2" {
  name   = "${var.prefix}-ubuntu.qcow2"
  pool   = var.libvirt_volume_pool
  source = var.libvirt_volume_source
  format = "qcow2"
}

module "cluster" {
  source = "../../"

  count = var.guest_count

  hostname            = format("${var.prefix}-%02d", count.index + 1)
  vcpu                = "4"
  memory              = "4096"
  libvirt_volume_pool = "gv0"
  libvirt_volume_size = 42949672960
  libvirt_volume_id   = libvirt_volume.ubuntu_qcow2.id
  network             = "ovs-network"
  port_group          = "v6-pub"

  # Cloud-Init using No-Cloud, see - https://cloudinit.readthedocs.io/en/latest/topics/datasources/nocloud.html#datasource-nocloud
  user_data = templatefile("${path.module}/templates/cloud_init.yml.tftpl", {
    user_name          = var.user_name
    user_passwd        = var.user_passwd
    ssh_authorized_key = var.ssh_authorized_key
    package_list       = var.package_list
  })
  meta_data = templatefile("${path.module}/templates/meta_data.yml.tftpl", {
    hostname = format("${var.prefix}-%02d", count.index + 1)
  })
  network_config = file("${path.module}/templates/network_config.yml")
}
