# Libvirt Variables
variable "libvirt_uri" {
  description = "URI of server running libvirtd"
  type        = string
  default     = "qemu:///system"
}

# Libvirt Instance Settings
variable "guest_count" {
  description = "Number of Guests to Create"
  type        = number
  default     = 3
}

variable "prefix" {
  description = "Resources will be prefixed with this to avoid clashing names"
  type        = string
  default     = "ubuntu"
}

variable "libvirt_volume_source" {
  description = "Volume Image Source"
  type        = string
  default     = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64-disk-kvm.img"
}

variable "libvirt_volume_pool" {
  description = "Volume Storage Pool"
  type        = string
  default     = "default"
}

# CloudInit Variables
variable "user_name" {
  description = "Host ssh user name"
  type        = string
  default     = "ubuntu"
}

variable "user_passwd" {
  description = "SSH user console password"
  type        = string
}

variable "ssh_authorized_key" {
  description = "SSH Public Key for user"
  type        = string
}

variable "package_list" {
  description = "List of additional packages to install"
  type        = list(string)
  default = [
    "qemu-guest-agent",
  ]
}
