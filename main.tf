terraform {
  required_version = "~> 1.4.0"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "~> 0.7.0"
    }
  }
}

resource "libvirt_volume" "ubuntu_qcow2_resized" {
  name           = "${var.hostname}.qcow2"
  base_volume_id = var.libvirt_volume_id
  pool           = var.libvirt_volume_pool
  size           = var.libvirt_volume_size
}

# for more info about paramater check this out
# https://github.com/dmacvicar/terraform-provider-libvirt/blob/master/website/docs/r/cloudinit.html.markdown
# Use CloudInit to add our ssh-key to the instance
# you can add also meta_data field
resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "${var.hostname}-seed.iso"
  pool           = var.libvirt_volume_pool
  user_data      = var.user_data
  meta_data      = var.meta_data
  network_config = var.network_config

}

# Create the machine
resource "libvirt_domain" "ubuntu" {
  name       = var.hostname
  memory     = var.memory
  vcpu       = var.vcpu
  qemu_agent = true
  cloudinit  = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name   = var.network
    wait_for_lease = true
  }

  # used to support features the provider does not allow to set from the schema
  xml {
    xslt = templatefile("${path.module}/templates/override.xsl", {
      network    = var.network
      port_group = var.port_group
    })
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.ubuntu_qcow2_resized.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

  lifecycle {
    ignore_changes = [
      # Ignore Perpetual Diff when using OpenVswitch
      network_interface,
    ]
  }
}
